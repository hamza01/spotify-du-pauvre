module Demo
{
    interface Server
    {
        void play(string nom, string music);
        void stop();
        void pause();
        string getUrl(string nom);
        string listSong();
        void deleteMusic(string file);
        void ChangeMusic(string oldNom, string newName);
        void loop(int h);
        void addMusic(string nom);
        void addMusic2(string path);
        string findFile(string nom);
        void shutdown();
    }
}
