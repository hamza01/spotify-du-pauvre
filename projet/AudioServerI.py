import vlc
from time import sleep
import Ice
import signal
import sys
import glob
from os import stat
import shutil
import os
from pathlib import Path
import socket 
Ice.loadSlice('AudioServer.ice')
import Demo


class AudioServerI(Demo.Server):

    audios = []
    global broadcast, sessionBuffer, vlcInstance, filepath, streamDest, streamUrl, streamStr, filepath
    sessionBuffer = bytes(str(id), 'ascii')
    vlcInstance = vlc.Instance()

    def __init__(self):
        self.ip = socket.gethostbyname(socket.gethostname())
        print("server available in ip: "+self.ip)
    
    def play(self, nom, music, current=None):

        print("play function ",nom, music)
        musicName = "./music/"+music+".mp3"
        streamStr= "#transcode{acodec=mp3,ab=128,channels=2,samplerate=44100}:http{dst=192.168.56.1:7778/"+str(nom)+".mp3}"
        vlc.libvlc_vlm_stop_media(vlcInstance, sessionBuffer)
        vlc.libvlc_vlm_release(vlcInstance)
        broadcast = vlc.libvlc_vlm_add_broadcast(vlcInstance, bytes(str(id), 'ascii'), bytes(musicName, 'utf-8'), bytes(streamStr, 'utf-8'),0, [], True, True)
        if(broadcast != 0):
            print("Error brodcasting !!!!!")
            return False
        
        
        a = vlc.libvlc_vlm_play_media(vlcInstance, bytes(str(id), 'ascii'))
        
        print("here ",a)
        return True

    def getUrl(self, nom, current=None):
        return "http://192.168.56.1:7778/"+str(nom)+".mp3"

    def listSong(self, current=None):
        #songs = glob.glob('./music/*.mp3')
        path = './music/' 
        files = [os.path.splitext(filename)[0] for filename in os.listdir(path)]
        return str(files)

    def stop(self,current=None):

        print("stop function")
        vlc.libvlc_vlm_stop_media(vlcInstance, sessionBuffer)
        vlc.libvlc_vlm_release(vlcInstance)
        print("Stream stoped by client")

    def pause(self,current=None):
        print("stop function")
        vlc.libvlc_vlm_pause_media(vlcInstance, bytes(str(id), 'ascii'))
        print("Stream paused by client")

    def ChangeMusic(self, oldNom, newName, current=None):
        old_file = os.path.join("./music/", oldNom)
        new_file = os.path.join("./music/", newName)
        os.rename(old_file, new_file)

    def deleteMusic(self, file, current=None):
        os.remove("./music/"+file+".mp3")

    def addMusic(self, nom, current=None):
        open("./music/"+nom, "w+")
        print(" Upload completed successfully! ")

    def addMusic2(self, path, current=None):
        shutil.move(path, './music/')
        print(" Upload completed successfully! ")


    def findFile(self, nom, current=None):
        
        for root, dirs, files in os.walk("./music/"):
            if nom in files:
                return os.path.join(root, nom)

        


    def shutdown(self, current):
        current.adapter.getCommunicator().shutdown()

