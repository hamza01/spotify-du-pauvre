import sys, Ice
Ice.loadSlice('AudioServer.ice')
import Demo
import AudioServerI
with Ice.initialize(sys.argv) as communicator:
    adapter = communicator.createObjectAdapterWithEndpoints("AudioServerAdapter", "tcp -h 192.168.56.1 -p 10000")
    object = AudioServerI.AudioServerI()
    adapter.add(object, communicator.stringToIdentity("Server"))
    adapter.activate()
    communicator.waitForShutdown()