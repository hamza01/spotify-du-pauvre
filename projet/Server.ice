//
// Copyright (c) ZeroC, Inc. All rights reserved.
//

#pragma once

module Demo
{
    interface Server
    {
        void play(int id);
        void stop();
        printMessage(String s)
    }
}
