import sys
import Ice
from time import sleep
import os
import subprocess
Ice.loadSlice('AudioServer.ice')
import Demo


def run(communicator):
    player = Demo.ServerPrx.checkedCast(
        communicator.stringToProxy("Server:default -h 192.168.56.1 -p 10000"))
    if not player:
        raise RuntimeError("invalid proxy")
        sys.exit(1)
    

   
    def all():
        menu()
        c = None
        global chiox, name
        # while c != 'x':
        playerOpen = False
        try:

            sys.stdout.write(" ==> ")
            sys.stdout.flush()
            c = sys.stdin.readline().strip()
            #a = sys.stdin.readline().strip()
            if c == 'p':
                print("here the list of songs we have : " + player.listSong())
                choix = str(input("choose the music you wanna hear  : "))
                name = str(input("Enter Your name please : "))
                player.play(name,choix)
                url = player.getUrl(name)
                if not playerOpen:
                        subprocess.Popen(["D:/Programes files/VLC/vlc.exe",url])
                print("click p to pause the music or s to stop it")
                action1 = sys.stdin.readline().strip()
                if action1 == 's':
                    player.stop()
                    print("the song is stoped by you !! ")
                if action1 == 'p':
                    player.pause()
                    print("the song is paused by you !! ")
                print("Tap p to play it again, r to return to the menu or x to exit !!")
                action2 = sys.stdin.readline().strip()
                if action2 == 'p':
                    player.play(name,choix)
                    print("Enjoy bye !!")
                    return all()
                if action2 == 'x':
                    player.shutdown()
                if action2 == 'r':
                    return all()
            

            elif c == 'd':
                print("here are the list of music : " + player.listSong())
                musicName = str(input("Enter the name of music please : "))
                player.deleteMusic(musicName)
                print("the music with name  " + musicName + " is deleted with sucess !! " + player.listSong())
                return all()
            elif c == 'c':
                print("here are the list of music : " + player.listSong())
                old = str(input("Enter the name of music you want to change : "))
                new = str(input("Enter the new name of music please : "))
                player.ChangeMusic(old+".mp3", new+".mp3")
                print("the music changed with success " + player.listSong())
                return all()
            elif c == 'a':
                addedMusicName = str(input("Enter the new name of music please : "))
                player.addMusic(addedMusicName+".mp3")
                print("Upload completed successfully!")
                print("here are the list of music : " + player.listSong())
                return all()
            elif c == 'o':
                print("here are the list of music before : " + player.listSong())
                musicPath = str(input("Enter the path of the new music please : "))
                player.addMusic2(musicPath)
                print("Upload completed successfully!")
                print("here are the list of music after : " + player.listSong())
                return all()
            elif c == 'f':
                music = str(input("Tap the music name you search about it : "))
                songs = player.findFile(music+".mp3")
                if songs :
                    print("The program found your music : " + songs)
                    print("If you want ot play this music Tap p : ")
                    action3 = sys.stdin.readline().strip()
                    if action3 == 'p':
                        name = str(input("Enter Your name please : "))
                        player.play(name,music)
                        url = player.getUrl(name)
                        if not playerOpen:
                            subprocess.Popen(["D:/Programes files/VLC/vlc.exe",url])
                        print("click p to pause the music, r to go to the menuc or s to stop it")
                        action5 = sys.stdin.readline().strip()
                        if action5 == 's':
                            player.stop()
                            print("the song is stoped by you !! ")
                        if action5 == 'p':
                            player.pause()
                            print("the song is paused by you !! ")
                        if action5 == 'r':
                            return all()
                                
                else:
                    print("Your music search about it doesn't exist")
                    return all()
            elif c == 'x':
                player.shutdown()
            


        except Ice.Exception as ex:
            print(ex)

    all()     


def menu():
    print("""

    Choose What you want please :

    p: Play ...

    d: Delete Music

    c: Change Music 

    a: Add Music 

    o: Add Music by Path

    f: find file

    x: Exit :(
    
    """)



with Ice.initialize(sys.argv) as communicator:

    if len(sys.argv) > 1:
        print(sys.argv[0] + ": too many arguments")
        sys.exit(1)

    run(communicator)
